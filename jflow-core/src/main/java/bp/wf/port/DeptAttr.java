package bp.wf.port;
import bp.en.EntityNoNameAttr;

/** 
 部门属性
*/
public class DeptAttr extends EntityNoNameAttr
{
	/** 
	 父节点编号
	*/
	public static final String ParentNo = "ParentNo";
	/** 
	 隶属组织
	*/
	public static final String OrgNo = "OrgNo";
}