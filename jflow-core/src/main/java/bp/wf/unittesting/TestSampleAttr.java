package bp.wf.unittesting;
import bp.en.EntityNoNameAttr;

/** 
 测试明细
*/
public class TestSampleAttr extends EntityNoNameAttr
{
	/** 
	 测试的API
	*/
	public static final String FK_API = "FK_API";
	/** 
	 版本
	*/
	public static final String FK_Ver = "FK_Ver";
	/** 
	 时间从
	*/
	public static final String DTFrom = "DTFrom";
	/** 
	 到
	*/
	public static final String DTTo = "DTTo";
	/** 
	 用了多少毫秒
	*/
	public static final String TimeUse = "TimeUse";
	/** 
	 每秒跑了多少个？
	*/
	public static final String TimesPerSecond = "TimesPerSecond";
}