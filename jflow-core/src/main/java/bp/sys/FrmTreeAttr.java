package bp.sys;

import bp.en.EntityTreeAttr;

/** 
 属性
*/
public class FrmTreeAttr extends EntityTreeAttr
{
	/** 
	 数据源
	*/
	public static final String DBSrc = "DBSrc";
	/** 
	 组织编号
	*/
	public static final String OrgNo = "OrgNo";
}