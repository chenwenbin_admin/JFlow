package bp.ccbill.template;

import bp.da.*;
import bp.en.*;
import bp.wf.port.*;
import bp.ccbill.*;
import java.util.*;

/** 
 单据查询岗位属性	  
*/
public class FrmStationDeptAttr
{
	/** 
	 表单
	*/
	public static final String FK_Frm = "FK_Frm";
	/** 
	 工作岗位
	*/
	public static final String FK_Station = "FK_Station";
	/** 
	 部门
	*/
	public static final String FK_Dept = "FK_Dept";
}